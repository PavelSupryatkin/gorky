﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zoom : MonoBehaviour {

    public Camera camera; 
	// Use this for initialization
	void Start () {
		
	}
    float len = 0;
	// Update is called once per frame
	void Update () {

        if (Input.touchCount == 2)
        {
            Vector2 v1 = Input.GetTouch(0).position;
            Vector2 v2 = Input.GetTouch(1).position;
            if (len == 0)
            {
                len = Vector2.Distance(v1, v2);
            }
            else
            {
                float f = len - Vector2.Distance(v1, v2);
                len = Vector2.Distance(v1,v2);
                camera.orthographicSize += f / 100f; 
            }
        }
        else {
            len = 0;
        }
        
        Debug.Log(len);
	}
}
